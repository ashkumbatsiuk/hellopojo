package com.hello;

import com.hello.pojos.v1_45.Transaction;
import org.beanio.*;
import org.beanio.builder.FixedLengthParserBuilder;
import org.beanio.builder.StreamBuilder;

public class Main {

    public static void main(String[] args) {

        // configure parser
        StreamFactory factory = StreamFactory.newInstance();
        StreamBuilder builder = new StreamBuilder("transaction")
                .format("fixedlength")
                .parser(new FixedLengthParserBuilder())
                .addRecord(Transaction.class);
        factory.define(builder);
        Unmarshaller unmarshaller = factory.createUnmarshaller("transaction");

        // parse object from string
        Transaction h = (Transaction) unmarshaller.unmarshal("abcdefghijklmnopqrstuvwxyz");

        System.out.print(h);
    }
}
