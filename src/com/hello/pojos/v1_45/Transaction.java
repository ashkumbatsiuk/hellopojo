package com.hello.pojos.v1_45;

import org.beanio.annotation.Field;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

@Record(minOccurs = 1, maxOccurs = 1)
public class Transaction {

    // filed w/o setter
    @Field(at = 1, length = 20, minOccurs = 1)
    private String id;

    public static class AddressT1 {
        @Field(at = 0, length = 3, minOccurs = 1)
        String str;
    }

    @Segment(at = 21, minOccurs = 1)
    private AddressT1 address;

    public String getId() {
        return id;
    }

    public AddressT1 getAddress() {
        return address;
    }
}
